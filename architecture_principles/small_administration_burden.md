# Architecture Principle

## Name

Small administration burden.

## Statement

CNO eliminates [toil](https://landing.google.com/sre/sre-book/chapters/eliminating-toil/) by automating all adminstration actions and applying [GitOps](https://www.gitops.tech/).

## Rationale

A small adminstation burden frees money for developing business functionality.

## Implications

Having tools for operation automation.

## Status

Draft.
