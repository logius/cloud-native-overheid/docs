# Architecture Principles

Architecture principles are expressed conform [TOGAF](https://pubs.opengroup.org/architecture/togaf9-doc/arch/chap20.html):

1. Name
2. Statement
3. Rationale
4. Implications

The following architecture principles are defined (under construction):

- [Cloud Agnostic](./cloud_agnostic.md)
- [Open Source, Open Standards](./open_source_open_standards.md)
- [Small administration burden](./small_administration_burden.md)
