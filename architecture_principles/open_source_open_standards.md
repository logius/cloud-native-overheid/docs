# Architecture Principle

## Name

Open Source, Open Standards.

## Statement

CNO is based on Open Source and Open Standards, conform the [Digitale Overheid](https://www.digitaleoverheid.nl/dossiers/oss-kennisnetwerk/).

## Rationale

Applying open source and open standards:

- Makes software transferable
- Makes software maintenance widely supported by an independent community rather than by vendor-bound product speciualists

## Implications

Software needs to be kept up to date with modern development.

## Status

Draft.
