# Architecture Principle

## Name

Cloud Agnostic.

## Statement

CNO remains Cloud Agnostic and avoids vendor lock-in:

- By applying "best of breed" Kubernetes-native tooling
- By having an opt-out for every tool selected

## Rationale

Platform instances of CNO can run in any Cloud, offered by any Cloud Provider (private, hybrid, public).

## Implications

Cloud Provider must conform to "aansluitvoorwaarden".

There is no public/hybrid cloud strategy by Dutch Government yet. Hence, at present CNO yet only runs in private cloud(s) of institutions within the Dutch Government.

## Status

Draft.
