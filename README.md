# Welcome to Cloud Native Overheid (CNO)

At Logius, we build and operate two cloud-native platforms, in close co-operation with the government data centres.

One platform is used by teams within Logius. The other platform is used government-wide. Both comprise our direct customers.

Join in and [get started](./getting-started.md) !

## Mission

*To build, maintain and operate a working environment for our customers.*

*To foster co-operation within the Dutch government on the topic "cloud native"*.

The working environment weights heavily when addressing issues.

## Vision

*Cloud Native Overheid is leveraged in every Government Datacentre.*

In the future also Hybrid Cloud.

## Strategy

In order to standardize development, deployment, operations and security, we take the [Cloud Native Interactive Landscape](https://landscape.cncf.io/) into account.

This leads to a [Cloud Native Overheid Landscape](./Cloud Native Overheid - Landscape.pptx).

## Organization

CNO is a Dutch-government initiative, presently joined by:

- Min. BZK / Logius:
  - Standard Platform (**SP**) (originating from Min. IenW)
  - Logius Private Cloud (**LPC**)
  - Overheids Datacenter Amsterdam (**ODC-A**)
- Min. OC&W:
  - Overheids Datacenter Noord (**ODC-N**)
  - Dienst Uitvoering Onderwijs (**DUO**)
- Min. J&V:
  - Centraal Justitieel Incassoburau (**CJIB**)

## Actors and Roles

CNO-actors are organized conform the [NIST Cloud Computing Standards Roadmap](https://www.nist.gov/publications/nist-cloud-computing-standards-roadmap):

- **Cloud Provider** and **Cloud Carrier**: deploys and operates an IaaS with managed Kubernetes.
> ODC-N, ODC-A, ...

- **Cloud Broker**: deploys and operates a PaaS with turn-key DevOps.
> Logius Standaard Platform, Logius Private Cloud, ...

- **Cloud Consumer**: deploys and operates applications on the platform, using the platform DevOps capabilities.
> Rijksoverheid, Municipalities, ...

These actors are assigned corresponding roles to perform their work:

- **Cloud Administrator**
- **Plaform Administrator**
- **Application Administrator**

These roles are used throughout the rest of the documentation to describe the various CNO-usages.

## Architecture

The CNO is depicted in a [technical architecture](./Cloud Native Overheid - Technical Architecture.pptx).

This architecture is further described by:

- **Architecture Principles**: a collection of agreed-upon [principles](./architecture_principles/README.md).
- **Architecture Decision Log**: a collection of agreed-upon [decisions](./architecture_decisions/README.md).

Solution architectures for actual deployment are out of scope for CNO. These are expected to be defined by the using  Cloud Providers and -Brokers.

## Guidelines

In CNO, the following guidelines are adopted:

- [Applying natural language](./guidelines/language.md)
- [Adding references](./guidelines/references.md)
- [Developing](./guidelines/development.md)
- [Testing](./guidelines/testing.md)
- [Versioning](./guidelines/versioning.md)
