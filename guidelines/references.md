# Guidelines for references

In sourcecode and commit messages, use references that have public- or CNO scope.

I.e. avoid making proprietary references such as references to team specific ticket numbers/ticketing systems.
