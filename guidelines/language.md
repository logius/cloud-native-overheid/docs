# Guidelines for language

Contents:

- Source code
- Git commits
- Issues
- Business terms

## Source code

(Comments in) source code are written in English.

## Git commits

Git commits are written in English.

## Issues

Issues are written in English.

## Business terms

Business terms in Dutch are respected.

For example:

*The Nederlandse Overheids Referentie Architectuur (NORA) is an architecture that describes ...*
