# Architecture Decision Log

The Architecture Decision Log is a collection of agreed-upon [Architecture Decision Records](https://adr.github.io/) (decision records).

There are various templates to express decision records. In CNO, the "simple and popular" [Michael Nygard template"](https://github.com/joelparkerhenderson/architecture_decision_record/blob/master/adr_template_by_michael_nygard.md) is used.

The following decision records are defined, having status:

- Decided
- In Review
- Candidate

# Decided

- [Policy engine](./policy_engine.md)
- [CVE process](./cve_process.md)

# In review

None at present.

# Candidate

- [Dockerless builds](./dockerless_builds.md)
- [Private registry](./private_registry.md)
- [Network policies](./network_policies.md)
- [(Sealed) secrets](./secrets.md)
